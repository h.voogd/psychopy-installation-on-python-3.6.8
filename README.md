This file contains a list to install Psychopy on Python 3.6.8 (32 bits).

Usage:
download PsychoPyDependenciesPython3.6.8.txt
open a command prompt (with administrator rights)
cd to the folder containing PsychoPyDependenciesPython3.6.8.txt
pip install -r PsychoPyDependenciesPython3.6.8.txt

Spyder is installed and can be started by typing 'spyder3'.


